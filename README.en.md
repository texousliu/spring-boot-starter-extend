# spring-boot-starter-extend

#### Description
自定义的一些spring-boot-starter, 主要是做一些工具类的封装。

#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

## 项目依赖说明
- [spring-boot-starter-extend starter扩展仓库][spring-boot-starter-extend]
- [commons 工具类仓库][commons]
- [archetype-stable Maven 骨架项目仓库][archetype-stable]
- [easy-talk 聊天仓库][easytalk]
- [open-demo 各种小例子仓库][open-demo]
- [web-parsing 网络爬虫仓库][web-parsing]
- [blog 博客文章仓库][blog]


[spring-boot-starter-extend]: https://gitee.com/texousliu/spring-boot-starter-extend
[commons]: https://gitee.com/texousliu/commons
[archetype-stable]: https://gitee.com/texousliu/archetype-stable
[easytalk]: https://gitee.com/texousliu/EasyTalkServer
[open-demo]: https://gitee.com/texousliu/open-demo
[web-parsing]: https://gitee.com/texousliu/WebParsingServer
[blog]: https://gitee.com/texousliu/blog