package cn.texous.starter.redis.config;

import cn.texous.starter.redis.RedisClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/23 9:33
 */
@Configuration
public class RedisAutoConfiguration {

    @Autowired(required = false)
    private StringRedisTemplate redisTemplate;

    @Bean
    public Object redisClient() throws Exception {
        RedisClient.init(redisTemplate);
        return new Object();
    }

}
