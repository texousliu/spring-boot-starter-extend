package cn.texous.starter.sms.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2019/10/14 11:34
 */
@Getter
@AllArgsConstructor
public enum DefaultPlatform implements Platform {
    /***/
    ALIYUN("aliyun", "aliyunSmsHandler", "阿里云短信平台"),
    /***/
    YUN_ZHI_XUN("yunZhiXun", "yunZhiXunSmsHandler", "云之讯短信平台"),
    /***/
    TENCENT("tencent", "tencentSmsHandler", "腾讯云短信平台"),
    ;

    private String platform;
    private String smsHandler;
    private String desc;

    @Override
    public String loadSmsHandler() {
        return smsHandler;
    }
}
