package cn.texous.starter.sms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 需要替换模板中变量的参数
 *
 * @author Showa.L
 * @since 2019/8/23 15:55
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SendTempParam {

    /** 变量所处的位置下标 */
    private String index;
    /** 变量名称 */
    private String key;
    /** 变量值 */
    private String value;

}
