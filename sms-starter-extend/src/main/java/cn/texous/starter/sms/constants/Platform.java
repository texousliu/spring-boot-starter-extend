package cn.texous.starter.sms.constants;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2019/10/14 11:26
 */
public interface Platform {

    /**
     * 获取平台消息发送实现类 bean 名称。
     *
     * @return
     */
    String loadSmsHandler();

}
