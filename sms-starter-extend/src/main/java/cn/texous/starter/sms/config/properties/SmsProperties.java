package cn.texous.starter.sms.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2019/10/14 9:35
 */
@Data
@Component
@ConfigurationProperties(prefix = "sms")
public class SmsProperties {

    private boolean autoSwitch;
    private TemplateProperties template;
    private AliyunSmsProperties aliyun = new AliyunSmsProperties();
    private YunZhiXunSmsProperties yunZhiXun = new YunZhiXunSmsProperties();
    private TencentSmsProperties tencent = new TencentSmsProperties();

    public boolean getAutoSwitch() {
        return autoSwitch;
    }

    public void setAutoSwitch(boolean autoSwitch) {
        this.autoSwitch = autoSwitch;
    }

}
