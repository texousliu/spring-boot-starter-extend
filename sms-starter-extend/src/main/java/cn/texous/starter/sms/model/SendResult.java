package cn.texous.starter.sms.model;

import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/23 10:31
 */
@Data
@ToString
public class SendResult {

    /***/
    public static final int SUCCESS = 1;

    private int code;
    private String channel;
    private String errorCode;
    private String errorMsg;
    private List<SendResult> errors = new ArrayList<>();

    public SendResult() {
    }

    public SendResult(String channel) {
        this.channel = channel;
    }

    public boolean success() {
        return this.code != 0;
    }

    public void addError(SendResult sendResult) {
        this.errors.add(sendResult);
    }

}
