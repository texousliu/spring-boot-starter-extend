package cn.texous.starter.sms.model;

import lombok.Data;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/23 15:19
 */
@Data
public class YunZhiXunResponse {

    private String code;
    private String msg;
    private String count;
    private String create_date;
    private String smsid;
    private String mobile;
    private String uid;

}
