package cn.texous.starter.sms.handler;

import cn.texous.starter.sms.model.SendInfo;
import cn.texous.starter.sms.model.SendResult;
import cn.texous.starter.sms.model.SendTempParam;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/23 10:50
 */
public interface SmsHandler {

    SendResult send(SendInfo sendInfo);

    String channel();

    boolean init();

    int order();

    /***/
    default String getParamStr(Map<String, String> params) {
        if (params == null || params.isEmpty())
            return null;
        return new Gson().toJson(params);
    }

    /***/
    default Map<String, String> parserToMap(List<SendTempParam> params, boolean isIndex) {
        if (params == null || params.isEmpty())
            return null;
        Map<String, String> result = new HashMap<>();
        if (isIndex) {
            params.forEach(p -> result.put(p.getIndex(), p.getValue()));
        } else
            params.forEach(p -> result.put(p.getKey(), p.getValue()));
        return result;
    }
}
