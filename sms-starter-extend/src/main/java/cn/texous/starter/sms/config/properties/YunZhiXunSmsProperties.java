package cn.texous.starter.sms.config.properties;

import lombok.Data;

/**
 * 阿里云 sms 短信发送配置
 *
 * @author Showa.L
 * @since 2019/4/23 20:31
 */
@Data
public class YunZhiXunSmsProperties {

    private int order = 0;
    private boolean init;
    private String domain;
    private String regionId;
    private String accessKeyId;
    private String accessSecret;

    public boolean getInit() {
        return init;
    }

    public void setInit(boolean init) {
        this.init = init;
    }
}
