package cn.texous.starter.sms.model;

import cn.texous.starter.sms.constants.DefaultPlatform;
import cn.texous.starter.sms.constants.Platform;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 短信发送参数。
 *
 * @author Showa.L
 * @since 2019/8/23 10:40
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SendInfo {

    /**
     * 国家（或地区）码 默认 86
     */
    private String nationCode;

    /**
     * 接收短信的手机号
     */
    private String mobile;

    /**
     * 短信平台代码
     *
     * <p>如果没有设置，则按配置中默认的顺序发送
     *
     * <p>如果配置中设置不进行自动切换，则使用排序中的第一顺位发送</p>
     *
     * <p>默认平台可使用 {@link DefaultPlatform}</p>
     *
     */
    private Platform platform;

    /**
     * 如果发送失败，是否切换平台发送。
     *
     * <p>如果配置文件中配置为全局配置，这边配置为本次发送配置，如果全局为 false 则该处配置无效.
     *
     */
    private boolean autoSwitch;

    /**
     * 短信发送模板 key
     * 对应配置：sms.template.*.${key}。${key} 代表templateCode
     */
    private String template;

    /**
     * 模板参数列表。
     *
     * <p>由于短信发送平台参数设置方式不一致（有些使用下标，有些使用key），
     * 因为要实现短信平台的切换发送，所以参数列表需要设置下标和key。
     *
     * <p>如果配置中没有设置为自动切换，则只需要设置对应短信平台所使用的方式。
     */
    private List<SendTempParam> params;

    public String getNationCode() {
        return nationCode == null ? "86" : nationCode;
    }
}
