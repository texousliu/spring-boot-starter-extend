package cn.texous.starter.sms.config.properties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2019/10/14 13:21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TemplateProperties {

    private Map<String, String> aliyun;
    private Map<String, String> yunZhiXun;
    private Map<String, String> tencent;

}
