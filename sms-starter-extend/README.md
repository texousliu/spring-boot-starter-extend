## 自定义 spring-boot-starter 实现

#### 已经实现的功能
- 阿里云短信发送
- 云之讯短信发送
- 发送渠道选择
- 发送渠道自动切换
- 自定义发送渠道

#### 后续需要开发
- 批量发送功能

#### 说明
- 如果需要实现自己的短信发送，请直接 编写bean类 实现 SmsHandler 接口。命名规范 XXXSmsHandler。
- 配置方式

```yaml
sms:
  # 开启发送失败时切换发送渠道
  auto-switch: false
  # 发送模板配置
  template:
    aliyun:
      # 模板 k-v 对应，不同平台之间，需要使用相同的 key，在切换平台时才能正确选择模板 
      # key 可以自行修改，value 对应平台的 template code
      login: qweqweq
      updatePwd: wdasd
    yun-zhi-xun:
      login: ewrqee
      updatePwd: loOKd
    
  # 阿里云短信配置
  aliyun:
    # 是否初始化该平台的短信发送
    init: false
    # 平台选择优先级，越小优先级越高
    order: 0
    domain: dysmsapi.aliyuncs.com
    action: SendSms
    version: 2017-05-25
    regionId: cn-hangzhou
    accessKeyId:
    accessSecret:
    signName:
  # 云之讯短信配置
  yun-zhi-xun:
    init: true
    order: 1
    # 短信发送域名: Rest Url 中的域名
    domain: open.ucpaas.com
    # 短信发送源id: Account Sid
    region-id: 
    # 短信发送appKey: AppID
    access-key-id: 
    # 短信发送secret: Auth Token
    access-secret: 
  tencent:
    init: false
    order: 2
    appid: 
    appkey: 
    sign-name: 
```
