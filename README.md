# spring-boot-starter-extend
#### 版本号更新说明
修改父类 pom 的版本号 parent.version 和 version  

使用命令更新子模块版本号:
```
mvn -N versions:update-child-modules
```

#### 已经集成
- redis-starter-extend: 主要封装了 RedisClient 工具类，更方便使用 redis
- sms-starter-extend: 主要集成各大短信平台，做到配置即用

#### 介绍
自定义的一些spring-boot-starter, 主要是做一些工具类的封装。

## 项目依赖说明
- [spring-boot-starter-extend starter扩展仓库][spring-boot-starter-extend]
- [commons 工具类仓库][commons]
- [archetype-stable Maven 骨架项目仓库][archetype-stable]
- [easy-talk 聊天仓库][easytalk]
- [open-demo 各种小例子仓库][open-demo]
- [web-parsing 网络爬虫仓库][web-parsing]
- [blog 博客文章仓库][blog]


[spring-boot-starter-extend]: https://gitee.com/texousliu/spring-boot-starter-extend
[commons]: https://gitee.com/texousliu/commons
[archetype-stable]: https://gitee.com/texousliu/archetype-stable
[easytalk]: https://gitee.com/texousliu/EasyTalkServer
[open-demo]: https://gitee.com/texousliu/open-demo
[web-parsing]: https://gitee.com/texousliu/WebParsingServer
[blog]: https://gitee.com/texousliu/blog